import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ServerComponent} from './server/server.component';

import {FormGroup, FormsModule} from '@angular/forms';
import {ServersComponent} from './servers/servers.component';
import {SuccessAlertComponent} from './success-alert/success-alert.component';
import {WarningAlartComponent} from './warning/warning-alart.component';
import {ReactiveFormsModule} from '@angular/forms';
import {StudentFormComponent} from './student-form/student-form.component';
import {BasicHighlightDirective} from './basic-highlight/basic-highlight.directive';
import {BetterdirectiveDirective} from './betterdirective.directive';
import {StructDirective} from './struct.directive';
import {HeaderComponent} from './header/header.component';
import {RecipesComponent} from './recipes/recipes.component';
import {RecipeListComponent} from './recipes/recipe-list/recipe-list.component';
import {RecipeDetailComponent} from './recipes/recipe-detail/recipe-detail.component';
import {RecipeItemComponent} from './recipes/recipe-item/recipe-item.component';
// import {ShoppingListComponent} from './shopping-list/shopping-list.component';
import {ShoppingEditComponent} from './shopping-list/shopping-edit/shopping-edit.component';
import {DropdownDirective} from './shared/dropdown.directive';
import {RouterModule, Routes} from '@angular/router';
import { FormEditorComponent } from './form-editor/form-editor.component';
import {HttpClientModule} from '@angular/common/http';

// @ts-ignore
// @ts-ignore
const appRoutes: Routes = [

  // {path: '/recipe', RecipesComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    ServerComponent,
    ServersComponent,
    SuccessAlertComponent,
    WarningAlartComponent,
    StudentFormComponent,
    BasicHighlightDirective,
    BetterdirectiveDirective,
    DropdownDirective,
    StructDirective,
    HeaderComponent,
    RecipesComponent,
    RecipeListComponent,
    RecipeDetailComponent,
    RecipeItemComponent,
    // ShoppingListComponent,
    ShoppingEditComponent,
    FormEditorComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {



}

