import {Component} from '@angular/core';
import {NewserviceService} from './newservice.service';
import {FormControl, Validators} from '@angular/forms';
import validate = WebAssembly.validate;
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  // providers: [NewserviceService]
})
export class AppComponent {
  // numbers = [1, 2, 3, 4, 5];
  // oddnumbers = [1 , 3, 5];
  // evennumbers = [ 2, 4];
  // onlyodd = false;
  // value = 100;
  // color: string;
  title = 'rajj';
  text: string;
  employees: any [] = [
    {
      code: 'emp001', name: 'raj', salary: 110020020, dob: '02/aug/1990'
    },
    {
      code: 'emp002', name: 'rajan', salary: 25761678, dob: '08/aug/1998'
    },
    {
      code: 'emp003', name: 'ravi', salary: 82891829, dob: '11/aug/1999'
    },
    {
      code: 'emp004', name: 'rajvi', salary: 9189928891, dob: '19/aug/1911'
    },

  ];

  // public name = 'Raj';
  //
  // loadedFeature = 'recipe';
  // onNavigate(feature: string) {
  //   this.loadedFeature = feature;
  // }
  // constructor(private _newservice: NewserviceService) {
  // }

  constructor(private http: HttpClient) {
  }


}



