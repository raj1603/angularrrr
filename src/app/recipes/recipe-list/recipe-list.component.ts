import { Component, OnInit } from '@angular/core';
import {Recipe} from '../recipes.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  recipes: Recipe[] = [new Recipe('recipe', 'this is simply a test2', 'https://upload.wikimedia.org/wikipedia/commons/1/15/Recipe_logo.jpeg'),
    new Recipe('recipe2', 'this is simply a test', 'https://logodix.com/logo/2178275.png'),
    new Recipe('recipe2', 'this is simply a test', 'https://logodix.com/logo/2178275.png')
  ];
  constructor() { }

  ngOnInit() {
  }

}
