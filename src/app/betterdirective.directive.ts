import {Directive, ElementRef, OnInit, Renderer2} from '@angular/core';

@Directive({
  selector: '[appBetterdirective]'
})
export class BetterdirectiveDirective implements OnInit {

  constructor(private elementRef: ElementRef, private renderer: Renderer2) { }
  //
  // ngOnInit() {
  //        this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'blue');
  // }

  ngOnInit() {
    this.elementRef.nativeElement.addEventListener('mouseenter', this.onMouseEnter);
  }
  onMouseEnter() {
    alert('enter!!!');
  }


}
