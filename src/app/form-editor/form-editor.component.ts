import {Component, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {validateWorkspace} from '@angular/cli/utilities/config';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-form-editor',
  templateUrl: './form-editor.component.html',
  styleUrls: ['./form-editor.component.css']
})
export class FormEditorComponent implements OnInit {

  SimpleForm: FormGroup;
  genders = ['male', 'female'];

  constructor(public http: HttpClient) {
  }

  ngOnInit() {


    this.SimpleForm = new FormGroup({
      name: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required, Validators.email]),
      gender: new FormControl('male'),
      hobbies: new FormArray([])

    });
  }

  Onsubmit() {
    console.log(this.SimpleForm);
  }

  onAddhobby() {
    const control = new FormControl(null, Validators.required);
    (<FormArray> this.SimpleForm.get('hobbies')).push(control);

  }

  onCreate(f) {
    //
    // const name = this.SimpleForm.value.name;
    // console.log(name);
    // const email = this.SimpleForm.value.email;
    // console.log(email);
    const data = this.SimpleForm.value;
    this.http.post('https://fir-1deff.firebaseio.com/addData.json', JSON.stringify(data)).subscribe(reponseData => {
      console.log(reponseData);
    });
  }

}

