import { Component, OnInit } from '@angular/core';
import {FormControl, FormBuilder , FormArray , Validators} from '@angular/forms';

@Component({
  selector: 'app-student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent {
  name = new FormControl('', Validators.required );
  lastname = new FormControl('', Validators.required);
  std = new FormControl('', Validators.required);
  gender = new FormControl('', Validators.required);
  dob = new FormControl('', Validators.required);
  checkbox = new FormControl('', Validators.required);
}
